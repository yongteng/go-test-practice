package providers

// Provider returns list of providers based on request planId
type Provider struct {
	FirstName string `json:"firstName" binding:"required"`
	LastName  string `json:"lastName" binding:"required"`
	Npi       string `json:"npi" binding:"required"`
}

// GetProviders ...
func GetProviders(firstName, lastName, planID string) []Provider {
	providerOne := Provider{FirstName: "Yongteng", LastName: "Huang", Npi: "ReallyCoolDrId"}
	providerTwo := Provider{FirstName: "Yiling", LastName: "Chen", Npi: "ReallyCoolDentistId"}
	providerThree := Provider{FirstName: "Jack", LastName: "GUttman", Npi: "ReallyCoolSpcecialist"}
	providerList := []Provider{providerOne, providerTwo, providerThree}
	return providerList
}
