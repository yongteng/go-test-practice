package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"test-practice/providers"
	"testing"

	"github.com/gin-gonic/gin"

	"github.com/stretchr/testify/assert"
)

type FakeBody struct {
	FirstName string `json:"PatientFirstName" binding:"required"`
	LastName  string `json:"PatientLastName" binding:"required"`
	PlanID    string `json:"planId" binding:"required"`
}

func TestHandleProviderList(t *testing.T) {
	resp := httptest.NewRecorder()

	b := FakeBody{
		FirstName: "Eyes",
		LastName:  "Huang",
		PlanID:    "999",
	}

	bs, err := json.Marshal(b)
	if err != nil {
		fmt.Println("err", err)
		return
	}
	body := bytes.NewBuffer(bs)

	gin.SetMode(gin.TestMode)
	ginTestContext, ginTestEngine := gin.CreateTestContext(resp)
	ginTestEngine.POST("/provider", handleProviderList)

	req, err := http.NewRequest(http.MethodPost, "/provider", body)
	req.Header.Add("Content-Type", "application/json")
	ginTestContext.Request = req
	handleProviderList(ginTestContext)
	//ginTestEngine.ServeHTTP(resp, req)
	result := resp.Result()

	assert := assert.New(t)
	assert.Nil(err)
	assert.Equal(http.StatusOK, result.StatusCode)

	providerOne := providers.Provider{FirstName: "Yongteng", LastName: "Huang", Npi: "ReallyCoolDrId"}
	providerTwo := providers.Provider{FirstName: "Yiling", LastName: "Chen", Npi: "ReallyCoolDentistId"}
	providerThree := providers.Provider{FirstName: "Jack", LastName: "GUttman", Npi: "ReallyCoolSpcecialist"}
	providerList := []providers.Provider{providerOne, providerTwo, providerThree}

	var providers []providers.Provider
	jsonBlob := []byte(resp.Body.String())
	fmt.Println(string(jsonBlob))
	err2 := json.Unmarshal(jsonBlob, &providers)
	if err2 != nil {
		fmt.Println("error:", err2)
	}
	assert.True(reflect.DeepEqual(providerList, providers))
	assert.Equal("[{\"firstName\":\"Yongteng\",\"lastName\":\"Huang\",\"npi\":\"ReallyCoolDrId\"},{\"firstName\":\"Yiling\",\"lastName\":\"Chen\",\"npi\":\"ReallyCoolDentistId\"},{\"firstName\":\"Jack\",\"lastName\":\"GUttman\",\"npi\":\"ReallyCoolSpcecialist\"}]", resp.Body.String())
}
