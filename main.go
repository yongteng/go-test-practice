package main

import (
	"net/http"
	"test-practice/providers"

	"github.com/gin-gonic/gin"
)

// send in parameters, ie: patient firstName lastName,
// cardholder id (plan information)
// returns list of providers based on his "shiznet"

func main() {
	r := gin.Default()
	r.POST("/provider", handleProviderList)

	r.Run()
}

func handleProviderList(c *gin.Context) {
	// Parse JSON
	var json struct {
		FirstName string `json:"PatientFirstName" binding:"required"`
		LastName  string `json:"PatientLastName" binding:"required"`
		PlanID    string `json:"planId" binding:"required"`
	}

	if c.Bind(&json) == nil {
		providerList := providers.GetProviders(json.FirstName, json.LastName, json.PlanID)
		c.JSON(http.StatusOK, providerList)
	}

}
